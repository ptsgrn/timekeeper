import { writable } from 'svelte/store';

export const isPause = writable<boolean>(false);
export const isStarted = writable<boolean>(false);
export const isFinished = writable<boolean>(false);
export const countdownToTime = writable<string>('');
export const timeDuration = writable<number>(0);
