import { initializeApp } from 'firebase/app';
import { GoogleAuthProvider, getAuth, onAuthStateChanged, signInWithPopup } from 'firebase/auth';
import 'firebase/firestore';
import { getDatabase, ref, set, onValue } from 'firebase/database';

const firebaseConfig = {
	apiKey: 'AIzaSyAvJSdy51pvcFzbRzrkamJrdeNECVltmqM',
	authDomain: 'patsagonesite.firebaseapp.com',
	databaseURL: 'https://patsagonesite-default-rtdb.asia-southeast1.firebasedatabase.app',
	projectId: 'patsagonesite',
	storageBucket: 'patsagonesite.appspot.com',
	messagingSenderId: '908887098198',
	appId: '1:908887098198:web:dc476095ccdf1b0ed24cf7',
	measurementId: 'G-GJWDG2KKM2'
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider();
export const db = getDatabase(app);
export { signInWithPopup, ref, set, onValue, onAuthStateChanged };
